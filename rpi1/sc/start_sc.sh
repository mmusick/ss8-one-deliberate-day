#!/bin/bash

port=57110
cont=true
# Turn the following sleep back on if this is a boot time script
# sleep 20

while $cont
  do
    #are we on a raspberry pi?
    if [ -f /etc/rpi-issue ]
      then
        # we need these two lines in order to make sound
        export SC_JACK_DEFAULT_INPUTS="system"
        export SC_JACK_DEFAULT_OUTPUTS="system"

        # get rid of anything already running
        killall scsynth
        sleep 2
        killall jackd

        # wait for things to settle down
        sleep 3

        # set the governer
        echo -n performance \
        | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

        sleep 3

        # start jackd
        ( jackd -p 32 -d alsa -d hw:0,0 -r 48000 -p 1024 -n 3  -s || sudo shutdown -r now ) &

        #wait for jack to settle down
        sleep 10

        # make sure we increment the port every loop
        port=$(( $port + 1))

        # start the server
        /usr/local/bin/scsynth -u $port & # check supercollider's directory!
        server=$!
        sleep 1
        echo $port

        # is the server running?
        if kill -0 $server  2> /dev/null
            then
                #all good
                echo "started on port: "

            else
                # try again

                sleep 5
                port=$(( $port + 1 ))
                /usr/local/bin/scsynth -u $port & # check supercollider's directory!
                server=$!

                sleep 1

                # is the server running?
                if kill -0 $server  2> /dev/null
                    then
                        #all good
                        echo "started"
                    else
                        sudo shutdown -r now
                fi
        fi

        sleep 5

        # is the server still running?
        if kill -0 $server  2> /dev/null
            then
                #all good
                echo "still going"
            else
                sudo shutdown -r now
        fi

        ###############################################################
        ###############################################################
        ## PLACE INSTALLATION FILES HERE FOR AUTO START!!!
        # /usr/local/bin/sclang startup.scd $port # check supercollider's directory!
        sclang
        ###############################################################
        

    else

        /path/to/sclang installation.scd #SuperCollider on your normal computer

    fi


    sleep 1

    killall scsynth
    sleep 2
    killall jackd
    cont=false
    echo "SuperCollider ended nicely. Bye Michael"
done
